/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.wpsj.model;

import java.util.List;

/**
 *
 * @author coivn
 */
public class ProductFinderBean {
    private String keyword;
    
    public void setKeyword(String keyword) {this.keyword = keyword;}
    
    public List<com.wpsj.entity.Product> getProducts() {
        return new com.wpsj.da.ProductDataAccess().getProductsByName(keyword);
    }

}
